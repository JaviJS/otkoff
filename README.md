Para ejecutar el servicio de IceStorm se debe abrir el directorio **/ice IceStorm**  
Luego ejecutar el siguiente comando:  
`icebox --Ice.Config=config.icebox`    

Para ejecutar el publicador se debe abrir el directorio **/otkoff_publisher**  
Luego ejecutar el siguiente comando:  
`python manage.py runserver`    

Los archivos relacionados con Ice por parte del publicador se encuentran en el directorio **/otkoff_publisher/otkoff_publisher/otkoff**    

Para ejecutar el suscriptor debe abrir el directorio que esta relacionado con ice que se encuentra en **/otkoff_subscriber/otkoff_subscriber/otkoff**  
Luego ejecutar el siguiente comando:  
`python subscriber.py`    

Pare ejecutar la plataforma web del suscriptor y poder visualizar los mangas, debe irse al siguiente directorio **/otkoff_subscriber**  
Luego ejecutar el siguiente comando:  
`python manage.py runserver`  