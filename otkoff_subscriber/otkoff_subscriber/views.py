from django.http import HttpResponse
from os import listdir
from django.views.decorators.csrf import csrf_exempt
import re
from django.shortcuts import render

"Este es el directorio en donde se iran almacenando todos los mangas que sean enviados desde el publicador"
url_principal='otkoff_subscriber/otkoff/mangas'

"""
Controlador para listar todos los mangas de un directorio
@param request Corresponde a una solicitud Http
return La plantilla listarMangas.html, junto con array que contiene todos los mangas almacenados en un directorio
""" 
def listarMangas(request):       
    return render(request,'listarMangas.html',{"mangas":carpetas(url_principal)})

"""
   Metodo para listar los archivos que tiene un directorio
   @param url Corresponde a la direccion de un directorio
   return El array documento con los archivos que contiene una url
""" 
def carpetas(url):
    "el metodo listDir lista los directorios que existen dentro de un url"
    documento= listdir(url)
    return documento

"""
   Controlador para listar todos los capitulos de un manga
   @param request Corresponde a una solicitud Http que recibe el nombre de un manga
   return La plantilla verManga.html, junto con array que contiene todos los capitulos de un manga, y el nombre del manga
""" 
@csrf_exempt
def listarCapitulosManga(request):
    "url de un manga"
    url_manga= url_principal+'/'+request.GET.get('nombreManga')

    "capitulos que contiene la carpeta de un manga"
    capitulos=carpetas(url_manga)

    "cantidad de capitulos que tiene un manga"
    cantCapitulos = len(capitulos)

  
    capituloManga=[]

    "si la cantidad de capitulos es distinto a 0"
    if (cantCapitulos!=0):
        "ciclo para recorrer todos los capitulos de un manga"
        for index, capitulo in enumerate(capitulos):
            "url de un capitulo de un manga"
            url_capitulo=url_manga+'/'+capitulo

            "buscar traductores que tiene un capitulo de un manga"
            carpeta_capitulo=carpetas(url_capitulo)

            "agrega el capitulo dentro del array capituloManga"
            capituloManga.append([capitulo]) 
            "agrega los traductores que tiene un capitulo dentro del array capituloManga"
            capituloManga[index].append(carpeta_capitulo)
    else:
        "si no hay capitulos se entrega un array vacio"    
        capituloManga=[]

    return render(request,'verManga.html',{"capitulos":capituloManga,"manga":request.GET.get('nombreManga')})

"""
   Controlador para ver un capitulo de manga
   @param request Corresponde a una solicitud Http
   return La plantilla verCapitulo.html, junto con array que contiene todas las imagenes de un capitulo
""" 
@csrf_exempt
def verCapitulo(request):    
    return render(request,'verCapitulo.html',cargarCapitulo(request.GET.get('manga')))

"""
   Metodo para buscar todas las imagenes de un capitulo de manga que ha subido un traductor
   @param mangaData Corresponde a un string que contiene el nombre del manga, el numero del capitulo y el traductor
   return Devuelve un array con los nombres de las imagenes, el nombre del manga, el numero del capitulo y el traductor
""" 
def cargarCapitulo(mangaData):
    "url de un capitulo de un manga subido por un traductor"
    url_manga= url_principal+'/'+mangaData
    "listar todas las imagenes que contiene un capitulo"
    carpeta_traductor=carpetas(url_manga)

    "separa en partes mangaData para obtener el nombre del manga, el numero del capitulo y el traductor"
    partes_manga=mangaData.split('/')

    "ordena el array con las imagenes de acuerdo a el numero de pagina de menor a mayor"
    r = re.compile(r"(\d+)")  
    carpeta_traductor.sort(key=lambda x: int(r.search(x).group(1)))
    return {"capitulo":carpeta_traductor,'manga':partes_manga[0],'numero_capitulo':partes_manga[1],'traductor':partes_manga[2]}

