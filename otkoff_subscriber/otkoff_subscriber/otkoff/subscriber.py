
import signal
import sys
import Ice
import IceStorm
import getopt
import base64
import os
import os.path as path
import shutil
Ice.loadSlice('Manga.ice')
import Demo


class CapituloI(Demo.Capitulo):  

    """
    Metodo para crear carpeta en donde llegara el capitulo de un manga de acuerdo a su traductor enviado desde el publicador al subscriptor
    @param capituloData Corresponde a los datos que tendra un capitulo, los cuales seran el nombre del manga, el numero del capitulo que se envia y 
    el traductor quien lo envia
    """  
    def publicarCapitulo(self, capituloData, current):    

        "Ruta donde ira almacenado un capitulo de manga enviado desde el publicador, la cual sera mangas/nombre_manga/numero_capitulo/traductor"	
        ruta='mangas/'+capituloData.nombreManga +"/" + str(capituloData.numeroCap)+ "/" + capituloData.traductor  
        
        "si la ruta existe, se borra la carpeta del traductor y se crea una nueva"
        if (path.exists(ruta)):
            shutil.rmtree(ruta)
            os.makedirs(ruta)
        else:    
            "Si la ruta no existe se crea la carpeta"
            os.makedirs(ruta)      

    """
    Metodo para agregar contenido a un archivo dentro de una carpeta especifica
    @param capituloData Corresponde a los datos que tendra un capitulo, los cuales seran el nombre del manga, el numero del capitulo que se envia y 
    el traductor quien lo envia
    @param imagenData Corresponde a los datos que tendra una imagen
    """  
    def agregarContenido(self, imagenData,capituloData, current):
        "imagen corresponde al nombre que tendra la imagen enviada desde el publicador"
        imagen=str(imagenData.pagina)+'.'+imagenData.extension;
        "ruta corresponde a la ruta en donde se guardara la imagen que se envia desde el publicador"
        ruta='mangas/'+capituloData.nombreManga +"/" + str(capituloData.numeroCap)+ "/" + capituloData.traductor  +'/'+imagen   

        "Si la ruta de la imagen enviada existe, se agregara el contenido de imagenData que corresponde a un array byte alfinal de la imagen"      
        if (path.exists(ruta)):
            f = open(ruta, "a")
            f.write(bytearray(imagenData.imagen))
            f.close()
        else:
            "si la ruta no existe se creara el archivo y se escribira el contenido enviado en imagenData"
            f = open(ruta, "wb")
            f.write(bytearray(imagenData.imagen))
            f.close()

def run(communicator):
 
    "nombre del topico que corresponde a los mangas que se enviaran"
    topicName = "manga"
    id = ""


    manager = IceStorm.TopicManagerPrx.checkedCast(communicator.propertyToProxy('TopicManager.Proxy'))
    if not manager:
        print(args[0] + ": invalid proxy")
        sys.exit(1)

    #
    # Retrieve the topic.
    #
    try:
        topic = manager.retrieve(topicName)
    except IceStorm.NoSuchTopic as e:
        try:
            topic = manager.create(topicName)
        except IceStorm.TopicExists as ex:
            print(sys.argv[0] + ": temporary error. try again")
            sys.exit(1)

    adapter = communicator.createObjectAdapter("Manga.Subscriber")

    #
    # Add a servant for the Ice object. If --id is used the identity
    # comes from the command line, otherwise a UUID is used.
    #
    # id is not directly altered since it is used below to detect
    # whether subscribeAndGetPublisher can raise AlreadySubscribed.
    #

    subId = Ice.Identity()
    subId.name = id
    if len(subId.name) == 0:
        subId.name = Ice.generateUUID()
    subscriber = adapter.add(CapituloI(), subId)

    #
    # Activate the object adapter before subscribing.
    #
    adapter.activate()

    qos = {} 

    try:
        topic.subscribeAndGetPublisher(qos, subscriber)
    except IceStorm.AlreadySubscribed:
        # This should never occur when subscribing with an UUID
        assert(id)
        print("reactivating persistent subscriber")

    communicator.waitForShutdown()

    #
    # Unsubscribe all subscribed objects.
    #
    topic.unsubscribe(subscriber)


#
# Ice.initialize returns an initialized Ice communicator,
# the communicator is destroyed once it goes out of scope.
#
with Ice.initialize(sys.argv, "config.sub") as communicator:
    #
    # Install a signal handler to shutdown the communicator on Ctrl-C
    #
    signal.signal(signal.SIGINT, lambda signum, frame: communicator.shutdown())
    if hasattr(signal, 'SIGBREAK'):
        signal.signal(signal.SIGBREAK, lambda signum, frame: communicator.shutdown())
    status = run(communicator)
