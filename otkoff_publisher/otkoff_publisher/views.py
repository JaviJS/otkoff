from django.http import HttpResponse, HttpResponseRedirect
from django.template import Template, Context
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from otkoff_publisher.otkoff.publisher import run

"""
	Controlador para la vista de inicio.
	@param request 
	@param mensaje String con un mensaje de estado (exito o error)

	return La plantilla html home.html
"""
@csrf_exempt
def home(request, mensaje=''):
	doc_home = open("otkoff_publisher/plantillas/home.html")
	plt = Template(doc_home.read())
	doc_home.close()

	ctx = Context({"mensaje": mensaje})
	documento = plt.render(ctx) 

	return HttpResponse(documento)

"""
	Controlador para publicar un manga.
	Llama al metodo run() del publisher.py

	@param request Solicitud que contiene los datos del manga mendiante el atributo POST y
	las imagenes del capito mediante el atributo FILES.

	return La plantilla html home.html con el mensaje success
"""
@csrf_exempt
def publicarManga(request):
	files = request.FILES.getlist('caps')

	nombreManga = request.POST.get('nombreManga')
	nombreTraductor = request.POST.get('nombreTraductor')
	numeroCapitulo = request.POST.get('numeroCapitulo')
	formatoCapitulo = request.POST.get('formatoCapitulo')

	caps = []

	for cap in files:
		caps.append(cap.read())
	
	print(len(caps))

	run(nombreManga, nombreTraductor, int(numeroCapitulo), caps, formatoCapitulo)


	return redirect('index', mensaje='success')
