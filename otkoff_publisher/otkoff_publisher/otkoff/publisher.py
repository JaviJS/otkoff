#!/usr/bin/env python
#
# Copyright (c) ZeroC, Inc. All rights reserved.
#

import signal
import sys
import time
import Ice
import IceStorm
import getopt
import base64

Ice.loadSlice('otkoff_publisher/otkoff/Manga.ice')
import Demo 

"""
    Metodo para dividir un Arraybytes en varios Arraybytes mas pequenSos.

    @param arrayImagen Corresponde a una imagen en Arraybyte.
    @param partesDivision Cantidad a dividir un Arraybyte.

    return Un Array con varios Arraybytes correspondientes a la Imagen
"""
def dividirImagen(arrayImagen, partesDivision):
    avg = len(arrayImagen) / float(partesDivision)
    out = []
    last = 0.0

    while last < len(arrayImagen):
        out.append(arrayImagen[int(last):int(last + avg)])
        last += avg

    return out

"""
    Metodo para establecer la conexion con IceStorm y publicar un manga.

    @param nombreManga String con el nombre de un manga.
    @param nombreTraductor String con el nombre del traductor del manga.
    @param numeroCapitulo Int correspondiente al numero del capituro del manga.
    @param caps Array de arraybytes correspondientes a las imagenes del capitulo.
    @param ext String correspondiente a la extension de las imagenes (.png, .jpg, etc).
"""
def run(nombreManga, nombreTraductor, numeroCapitulo, caps, ext):

    #Creacion de conexion
    communicator = Ice.initialize(sys.argv, "otkoff_publisher/otkoff/config.pub")

    #Crecion de topico Manga
    topicName = "manga"

    manager = IceStorm.TopicManagerPrx.checkedCast(communicator.propertyToProxy('TopicManager.Proxy'))
    if not manager:
        print(args[0] + ": invalid proxy")
        sys.exit(1)
    try:
        topic = manager.retrieve(topicName)
    except IceStorm.NoSuchTopic:
        try:
            topic = manager.create(topicName)
        except IceStorm.TopicExists:
            print(sys.argv[0] + ": temporary error. try again")
            sys.exit(1)

    publisher = topic.getPublisher()

    manga = Demo.CapituloPrx.uncheckedCast(publisher)


    #Logica
    try:

        #Creacion de CapituloData, se le agrega el nombre, numero y traductor del capitulo.
        capitulo = Demo.CapituloData()
        capitulo.nombreManga = nombreManga
        capitulo.numeroCap = numeroCapitulo
        capitulo.traductor = nombreTraductor

        #Llamar al metodo publicarCapitulo(), hay que pasarle un CapituloData.
        #El metodo crea el directorio donde se guardara el capitulo nombreManga/numeroCapitulo/nombreTraductor
        manga.publicarCapitulo(capitulo)

        #Key corresponde a un indice para el for
        key = 1
        #For que reccorre el Array caps
        for cap in caps:
            #Calcular el tamano de array cap que es parte del array caps.
            cap_len = len(cap)
            #llamar al metodo dividirImagen() y guardar lo que retorna en la variable cap_arr (Devuelve un array de arrays que corresponden a 1 imagen)
            cap_arr = dividirImagen(cap, cap_len/500)
            #Se guarda la extension en una variable y la key que corresponderia a un numero de pagina (Dando por hecho que las imagenes vienen en orden)
            extension = ext
            numPagina = key

            #For que recorre el array de cap_arr, contiene una imagen dividida en muchas partes
            for page in cap_arr:
                #Se crea una ImagenData, se le agrega el numero de pagina, la extension y una parte de la imagen.
                img = Demo.ImagenData()
                img.pagina = numPagina
                img.extension = extension
                img.imagen = page 

                #llama al metodo agregarContenido() que crea un arhivo de imagen y la agrega la data 
                #En caso de que el archivo ya exista, agrega la data al final del contenido ya existente
                #Para entenderlo de una mejor manera quiere decir que va agregando partes de la imagen a un archivo
                #Hasta formar la imagen en su totalidad
                manga.agregarContenido(img, capitulo) 

            #Imprimir datos en la consola para verificar cuando se ternima de neviar una imagen
            time.sleep(0.05) 
            key = key + 1
            print(numPagina)

        communicator.destroy()
    except IOError:
        # Ignore
        pass
    except Ice.CommunicatorDestroyedException:
        # Ignore
        pass
