module Demo
{
    sequence<byte> Imagen;

    struct ImagenData{
        int pagina;
        string extension;
        Imagen imagen;
    }

    struct CapituloData
    {
        string nombreManga;
        int numeroCap;
        string traductor;
    }


    interface Capitulo{

        void publicarCapitulo(CapituloData capitulo);
        void agregarContenido(ImagenData byteImagen, CapituloData capitulo); 
    }
}